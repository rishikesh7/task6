import React from 'react';
import './priceCard.css';

interface Props {
	header: string,
	price: number,
	content: Array<string>,
	buttonText: string,
	featured: boolean,
	changeFeatured(): void 
}

const PriceCard: React.FC<Props> = ({ header,price,content,buttonText,featured,changeFeatured}) => {
	return (
		<div className={"pricecard"} onClick={changeFeatured}>
			<div className={"pricecard-header"}>
				<div>{header}</div>
				{ featured && (
					<>
						<div className={"small-text"}>Most popular</div>
						<div className={"star"}>&#9734;</div>
					</> 
				)}
			</div>
			<div className={"pricecard-content"}>
				<div className={"pricecard-content-price"}><span className={"price"}>${price}</span>/mo</div>
				<div className={"pricecard-content-description"}>
					{
						content.map((item, ix) => <div className={"pricecard-content-description-item"} key={ix}>{item}</div>)
					}
				</div>
				<div className={`${featured ? "pricecard-button-featured" : "pricecard-button"}`}>
					<div className={`${featured ? "pricecard-button-featured-text" : "pricecard-button-text"}`}>{buttonText}</div>
				</div>
			</div>
		</div>
	)
}

export default PriceCard;