import React from 'react';
import './footer.css';

const Footer : React.FC = () => {
	return (
		<>
			<div className={"footer-section"}>
				<ul className={"footer-section-content"}>
					<li className={"footer-item-main"}>Company</li>
					<li className={"footer-item"}>Team</li>
					<li className={"footer-item"}>Contact us</li>
					<li className={"footer-item"}>History</li>
					<li className={"footer-item"}>Locations</li>
				</ul>
			</div>
			<div className={"footer-section"}>
				<ul className={"footer-section-content"}>
					<li className={"footer-item-main"}>Features</li>
					<li className={"footer-item"}>Cool stuff</li>
					<li className={"footer-item"}>Random feature</li>
					<li className={"footer-item"}>Team feature</li>
					<li className={"footer-item"}>Developer stuff</li>
					<li className={"footer-item"}>Another one</li>
				</ul>
			</div>
			<div className={"footer-section"}>
				<ul className={"footer-section-content"}>
					<li className={"footer-item-main"}>Resources</li>
					<li className={"footer-item"}>Team</li>
					<li className={"footer-item"}>Contact us</li>
					<li className={"footer-item"}>History</li>
					<li className={"footer-item"}>Locations</li>
				</ul>
			</div>
			<div className={"footer-section"}>
				<ul className={"footer-section-content"}>
					<li className={"footer-item-main"}>Legal</li>
					<li className={"footer-item"}>Privacy policy</li>
					<li className={"footer-item"}>Terms of use</li>
				</ul>
			</div>
		</>
	)
} 

export default Footer;