import React from 'react';
import './header.css';

const Header : React.FC = () => {
	return (
		<div className={"heading"}>
			<div className={"heading-text"}>Pricing</div>
			<div className={"heading-description"}>Quickly build an effective pricing table for your potential customers with this layout. It's built with default Material-UI components with little customization.</div>
		</div>
	)
} 

export default Header;