import React from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './components/header/header';
import PriceCard from './components/priceCard/priceCard';
import Footer from './components/footer/footer';

interface Data {
  header: string,
  price: number,
  content: Array<string>,
  buttonText: string,
  // featured: boolean
}

const data: Array<Data> = [
  {
    header: "Free",
    price: 0,
    content: [
      "10 users included",
      "2GB of storage",
      "Help center access",
      "Email support"
    ],
    buttonText: "SIGN UP FOR FREE",
    // featured: false,
  },
  {
    header: "Pro",
    price: 15,
    content: [
      "20 users included",
      "10GB of storage",
      "Help center access",
      "Priority email support"
    ],
    buttonText: "GET STARTED",
    // featured: true,
  },
  {
    header: "Enterprise",
    price: 30,
    content: [
      "50 users included",
      "30GB of storage",
      "Help center access",
      "Phone and Email support"
    ],
    buttonText: "CONTACT US",
    // featured: false,
  },
]

interface Props {}

interface State {
  featuredNo: number;
}


class App extends React.Component<Props,State> {

  constructor(props: Props){
    super(props)
    this.state = {
      featuredNo: 1
    }
  }

  changeFeatured = (newFeatured: number)  => {
    this.setState({ featuredNo: newFeatured });
  }

  render(){

    const { featuredNo } = this.state;

    return (
      <div className="App">
        <Header />
        <div className={"pricecards"}>
          { 
            data.map((card, ix) => 
              <PriceCard {...card} 
                key={card.header} 
                featured={ix === featuredNo} 
                changeFeatured={() => this.changeFeatured(ix)}
              />
          )}
        </div>
        <hr className={"divider"} />
        <div className={"footer"}>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
